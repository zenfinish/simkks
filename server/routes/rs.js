const Router = require('express').Router();

const RsController = require('../controllers/rs.js');

Router.get('/all', RsController.getAll);

Router.put('/tarif', RsController.putTarif);

module.exports = Router;
