const Router = require('express').Router();

const LaporanController = require('../controllers/laporan.js');

Router.get('/:bulan/minggu1/:department_id', LaporanController.getBulanMinggu1);
Router.get('/:bulan/minggu2/:department_id', LaporanController.getBulanMinggu2);
Router.get('/:bulan/minggu3/:department_id', LaporanController.getBulanMinggu3);
Router.get('/:bulan/minggu4/:department_id', LaporanController.getBulanMinggu4);

module.exports = Router;
