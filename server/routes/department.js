const Router = require('express').Router();

const DepartmentController = require('../controllers/department.js');

Router.get('/all', DepartmentController.getAll);

module.exports = Router;
