const router = require('express').Router();

const laporanRoute = require('./laporan.js');
const departmentRoute = require('./department.js');
const areaRoute = require('./area.js');
const positionRoute = require('./position.js');
const mahasiswaRoute = require('./mahasiswa.js');
const rsRoute = require('./rs.js');
const dokterRoute = require('./dokter.js');
const userRoute = require('./user.js');

const { isUser } = require('../middlewares/user.js');

router.use('/laporan', isUser, laporanRoute);
router.use('/department', isUser, departmentRoute);
router.use('/area', isUser, areaRoute);
router.use('/position', isUser, positionRoute);
router.use('/mahasiswa', isUser, mahasiswaRoute);
router.use('/rs', isUser, rsRoute);
router.use('/dokter', isUser, dokterRoute);
router.use('/user', userRoute);

router.get('*', function(req, res){
  res.status(404).json('Link tidak ditemukan');
});

module.exports = router;
