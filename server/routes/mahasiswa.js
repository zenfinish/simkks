const Router = require('express').Router();

const MahasiswaController = require('../controllers/mahasiswa.js');

Router.get('/search', MahasiswaController.getSearch);
Router.get('/stase/:npm', MahasiswaController.getStaseNpm);
Router.post('/stase', MahasiswaController.postStase);
Router.put('/stase', MahasiswaController.putStase);
Router.put('/stase/new', MahasiswaController.putStaseNew);
Router.delete('/stase/:id_zstase', MahasiswaController.deleteStaseIdZstase);

module.exports = Router;
