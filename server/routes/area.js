const Router = require('express').Router();

const AreaController = require('../controllers/area.js');

Router.get('/all', AreaController.getAll);

module.exports = Router;
