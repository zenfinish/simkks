const Router = require('express').Router();

const PositionController = require('../controllers/position.js');

Router.get('/all', PositionController.getAll);

module.exports = Router;
