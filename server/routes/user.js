const Router = require('express').Router();
const { isUser, cekEmail, cekNpm } = require('../middlewares/user.js');
const UserController = require('../controllers/user.js');

Router.post('/login', cekEmail, UserController.postLogin);
Router.post('/login/mahasiswa', cekNpm, UserController.postLoginMahasiswa);
Router.get('/cektoken', isUser, UserController.getCektoken);
Router.put('/mahasiswa/password', isUser, UserController.putMahasiswaPassword);

module.exports = Router;
