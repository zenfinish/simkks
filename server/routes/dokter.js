const Router = require('express').Router();

const DokterController = require('../controllers/dokter.js');

Router.get('/all', DokterController.getAll);
Router.get('/nilai', DokterController.getNilai);
Router.get('/nilai/detil/:id_znilai', DokterController.getNilaiDetilIdZNilai);

Router.post('/', DokterController.post);
Router.post('/nilai', DokterController.postNilai);

module.exports = Router;
