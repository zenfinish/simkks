const { verifyToken } = require('../configs/jsonwebtoken');
const mysql = require('../configs/mysql.js');

module.exports = {

	cekEmail(req, res, next) {
		mysql.query(`
			SELECT
				id AS id_zuser,
				nama AS nama_zuser,
				password
			FROM z_user
			WHERE email = '${req.body.email}'
		`, function(error, result) {
			if (error) {
				res.status(500).json(error);
			} else {
				if (result[0] === undefined) {
					res.status(400).json({ ket: 'Email Tidak Terdaftar.' });
				} else {
					req.body['sandi'] = result[0].password;
					req.body['id_zuser'] = result[0].id_zuser;
					req.body['nama_zuser'] = result[0].nama_zuser;
					next();
				}
			}
		});
	},

	cekNpm(req, res, next) {
		mysql.query(`
			SELECT
				first_name AS nama_personnelemployee
			FROM personnel_employee
			WHERE emp_code = '${req.body.npm}' && password = '${req.body.password}'
		`, function(error, result) {
			if (error) {
				res.status(500).json(error);
			} else {
				if (result[0] === undefined) {
					res.status(400).json({ ket: 'NPM Tidak Terdaftar. / Password Salah' });
				} else {
					req.body['nama_personnelemployee'] = result[0].nama_personnelemployee;
					next();
				}
			}
		});
	},

	isUser(req, res, next) {
		verifyToken(req.headers.token, function(err, decoded) {
			if (err) {
				res.status(400).json({ ket: 'Token Anda Tidak Sah' });
			} else {
				req.user = decoded;
				next();
			}
		});
	},

}