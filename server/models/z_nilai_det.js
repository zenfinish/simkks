class ZNilaiDet {

	static saveMany(connection, data) {
		return new Promise((resolve, reject) => {
			let sql = "INSERT INTO z_nilai_det(id_znilai, emp_id, nilai) VALUES ?";
			connection.query(sql, [data], function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByIdZnilai(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.nilai,
					b.first_name,
					b.last_name,
					b.emp_code AS npm
				FROM z_nilai_det a
				LEFT JOIN personnel_employee b ON a.emp_id = b.id
				WHERE a.id_znilai = '${data.id_znilai}'
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = ZNilaiDet;
