class ZUser {

	static saveOne(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				INSERT INTO z_user (nama, password, email)
				VALUES ('${data.nama_zuser}', '$2y$12$SUKx41dw/q38MUvHE.ML6.uY1MBcgHFXCHoKAJQfBtu4UsrtJPkFq', '${data.email}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = ZUser;
