class PersonnelDepartment {

	static findAll(connection) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id AS department_id,
					a.dept_name
				FROM personnel_department a
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = PersonnelDepartment;
