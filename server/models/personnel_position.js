class PersonnelPosition {

	static findAll(connection) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id AS position_id,
					a.position_name
				FROM personnel_position a
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = PersonnelPosition;
