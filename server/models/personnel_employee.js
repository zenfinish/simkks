class PersonnelEmployee {

	static findBySearch(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id AS emp_id,
					a.first_name,
					a.last_name,
					a.emp_code AS npm,
					a.position_id,
					b.dept_name AS kampus,
					c.position_name AS stase
				FROM personnel_employee a
				LEFT JOIN personnel_department b ON a.department_id = b.id
				LEFT JOIN personnel_position c ON a.position_id = c.id
				WHERE a.emp_code LIKE '%${data.search}%' || a.first_name LIKE '%${data.search}%' || a.last_name LIKE '%${data.search}%'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateByPositionId(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				UPDATE personnel_employee
				SET position_id = '${data.position_id}'
				WHERE id = '${data.emp_id}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updatePasswordByEmpId(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				UPDATE personnel_employee SET
					password = '${data.password}'
				WHERE emp_code = '${data.emp_code}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = PersonnelEmployee;
