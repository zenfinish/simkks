class ZNilai {

	static findByIdZuser(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id AS id_znilai,
					DATE_FORMAT(a.tgl, '%Y-%m-%d %T') AS tgl,
					b.area_name,
					c.nama AS nama_zuser,
					c.email,
					d.position_name
				FROM z_nilai a
				LEFT JOIN personnel_area b ON a.area_id = b.id
				LEFT JOIN z_user c ON a.id_zuser = c.id
				LEFT JOIN personnel_position d ON a.position_id = d.id
				WHERE a.id_zuser = '${data.id_zuser}'
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static saveOne(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				INSERT INTO z_nilai (id_zuser,	area_id, position_id)
				VALUES ('${data.id_zuser}', '${data.area_id}', '${data.position_id}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = ZNilai;
