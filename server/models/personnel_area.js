class PersonnelArea {

	static findAll(connection) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id AS area_id,
					a.area_name,
					(
						SELECT tarif FROM z_tarif WHERE name_personnelarea = a.area_name ORDER BY id DESC LIMIT 1
					) AS tarif
				FROM personnel_area a
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = PersonnelArea;
