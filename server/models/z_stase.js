class ZStase {

	static findByEmpCode(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id AS id_zstase,
					DATE_FORMAT(a.tgl_masuk, '%Y-%m-%d') AS tgl_masuk,
					a.id_personnelarea,
					a.id_personnelposition,
					a.id_zdokter,

					b.area_name AS nama_personnelarea,

					c.position_name AS nama_personnelposition,

					d.nama AS nama_zdokter
				FROM z_stase a
				LEFT JOIN personnel_area b ON a.id_personnelarea = b.id
				LEFT JOIN personnel_position c ON a.id_personnelposition = c.id
				LEFT JOIN z_user d ON a.id_zdokter = d.id
				WHERE a.emp_code = '${data.emp_code}'
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static createOne(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				INSERT INTO z_stase (emp_code, id_personnelarea, id_personnelposition, id_zdokter, tgl_masuk)
				VALUES ('${data.emp_code}', '${data.id_personnelarea}', '${data.id_personnelposition}', '${data.id_zdokter}', '${data.tgl_masuk}')
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updateByidZstase(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				UPDATE z_stase SET
					id_personnelarea = '${data.id_personnelarea}',
					id_personnelposition = '${data.id_personnelposition}',
					id_zdokter = '${data.id_zdokter}',
					tgl_masuk = '${data.tgl_masuk}'
				WHERE id = '${data.id_zstase}'
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static deleteByidZstase(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				DELETE FROM z_stase WHERE id = '${data.id_zstase}'
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = ZStase;
