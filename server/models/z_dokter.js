class ZDokter {

	static findAll(connection) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id_zuser,
					a.hp,
					b.area_name,
					c.nama AS nama_zuser,
					c.email
				FROM z_dokter a
				LEFT JOIN personnel_area b ON a.area_id = b.id
				LEFT JOIN z_user c ON a.id_zuser = c.id
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByIdZuser(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id_zuser,
					a.hp,
					a.area_id,
					b.area_name,
					c.nama AS nama_zuser,
					c.email
				FROM z_dokter a
				LEFT JOIN personnel_area b ON a.area_id = b.id
				LEFT JOIN z_user c ON a.id_zuser = c.id
				WHERE id_zuser = '${data.id_zuser}'
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static saveOne(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				INSERT INTO z_dokter (id_zuser, hp, area_id)
				VALUES ('${data.id_zuser}', '${data.hp}', '${data.area_id}')
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = ZDokter;
