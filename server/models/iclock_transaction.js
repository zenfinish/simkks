class IclockTransaction {

	static findByMahasiswaPerMinggu(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.punch_time AS tgl
				FROM iclock_transaction a
				LEFT JOIN personnel_employee b ON a.emp_id = b.id
				WHERE
					(a.punch_time BETWEEN CONCAT('${data.bulan}', '-', '${data.mingguAwal}') AND CONCAT('${data.bulan}', '-', '${data.mingguAkhir}', ' 23:59:59'))
					&& a.emp_id = '${data.emp_id}'
				GROUP BY SUBSTRING(a.punch_time, 1, 10)
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
	static findMahasiswaPerMingguByDepartmentId(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					CONCAT('${data.mingguAwal}-', '${data.bulan}', ' s/d ', '${data.mingguAkhir}-', '${data.bulan}') AS periode,	
					c.dept_name AS kampus,
					CONCAT(COALESCE(b.first_name, ''), ' ', COALESCE(b.last_name, '')) AS nama,
					b.emp_code AS npm,
					a.emp_id,
					e.area_name AS rumah_sakit,
					f.position_name AS stase,
					d.tarif
				FROM iclock_transaction a
				LEFT JOIN personnel_employee b ON a.emp_id = b.id
				LEFT JOIN personnel_department c ON b.department_id = c.id
				LEFT JOIN z_tarif d ON a.area_alias = d.name_personnelarea
				LEFT JOIN personnel_area e ON d.name_personnelarea = e.area_name
				LEFT JOIN personnel_position f ON a.id_personnelposition = f.id
				WHERE
					a.punch_time BETWEEN CONCAT('${data.bulan}', '-', '${data.mingguAwal}') AND CONCAT('${data.bulan}', '-', '${data.mingguAkhir}', ' 23:59:59')
					&& b.department_id = '${data.department_id}'
				GROUP BY a.emp_id
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
  }
	
}

module.exports = IclockTransaction;
