class ZTarif {

	static saveOne(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				INSERT INTO z_tarif (tarif, name_personnelarea)
				VALUES ('${data.tarif}', '${data.name_personnelarea}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = ZTarif;
