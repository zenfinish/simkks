const bcrypt = require('bcryptjs');

module.exports = {

	hashingPassword(password) {
		return bcrypt.hashSync(password, bcrypt.genSaltSync());
	},

	comparePassword(password, sandi) {
		return bcrypt.compareSync(password, sandi);
	},

};
