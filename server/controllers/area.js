const PersonnelArea = require('../models/personnel_area.js');
const mysql = require('../configs/mysql.js');

class AreaController {

	static getAll(req, res) {
		PersonnelArea.findAll(mysql)
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }

};

module.exports = AreaController;
