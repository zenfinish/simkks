const PersonnelPosition = require('../models/personnel_position.js');
const mysql = require('../configs/mysql.js');

class PositionController {

	static getAll(req, res) {
		PersonnelPosition.findAll(mysql)
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }

};

module.exports = PositionController;
