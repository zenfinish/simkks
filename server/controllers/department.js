const PersonnelDepartment = require('../models/personnel_department.js');
const mysql = require('../configs/mysql.js');

class DepartmentController {

	static getAll(req, res) {
		PersonnelDepartment.findAll(mysql)
		.then(async function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }

};

module.exports = DepartmentController;
