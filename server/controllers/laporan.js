const IclockTransaction = require('../models/iclock_transaction.js');
const mysql = require('../configs/mysql.js');

class LaporanController {

	static getBulanMinggu1(req, res) {
		IclockTransaction.findMahasiswaPerMingguByDepartmentId(mysql, {
      bulan: req.params.bulan,
      department_id: req.params.department_id,
      mingguAwal: '01',
      mingguAkhir: '07'
    })
		.then(async function(result) {
      for (let i = 0; i < result.length; i++) {
        await IclockTransaction.findByMahasiswaPerMinggu(mysql, {
          emp_id: result[i].emp_id,
          bulan: req.params.bulan,
          mingguAwal: '01',
          mingguAkhir: '07'
        })
        .then(function(result2) {
          result[i]['data'] = result2;
        })
        .catch(function(error) {
          return res.status(500).json(error);
        });
      }
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }
  
  static getBulanMinggu2(req, res) {
		IclockTransaction.findMahasiswaPerMingguByDepartmentId(mysql, {
      bulan: req.params.bulan,
      department_id: req.params.department_id,
      mingguAwal: '08',
      mingguAkhir: '15'
    })
		.then(async function(result) {
      for (let i = 0; i < result.length; i++) {
        await IclockTransaction.findByMahasiswaPerMinggu(mysql, {
          emp_id: result[i].emp_id,
          bulan: req.params.bulan,
          mingguAwal: '08',
          mingguAkhir: '15'
        })
        .then(function(result2) {
          result[i]['data'] = result2;
        })
        .catch(function(error) {
          return res.status(500).json(error);
        });
      }
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }
  
  static getBulanMinggu3(req, res) {
		IclockTransaction.findMahasiswaPerMingguByDepartmentId(mysql, {
      bulan: req.params.bulan,
      department_id: req.params.department_id,
      mingguAwal: '16',
      mingguAkhir: '23'
    })
		.then(async function(result) {
      for (let i = 0; i < result.length; i++) {
        await IclockTransaction.findByMahasiswaPerMinggu(mysql, {
          emp_id: result[i].emp_id,
          bulan: req.params.bulan,
          mingguAwal: '16',
          mingguAkhir: '23'
        })
        .then(function(result2) {
          result[i]['data'] = result2;
        })
        .catch(function(error) {
          return res.status(500).json(error);
        });
      }
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }
  
  static getBulanMinggu4(req, res) {
		IclockTransaction.findMahasiswaPerMingguByDepartmentId(mysql, {
      bulan: req.params.bulan,
      department_id: req.params.department_id,
      mingguAwal: '24',
      mingguAkhir: '31'
    })
		.then(async function(result) {
      for (let i = 0; i < result.length; i++) {
        await IclockTransaction.findByMahasiswaPerMinggu(mysql, {
          emp_id: result[i].emp_id,
          bulan: req.params.bulan,
          mingguAwal: '24',
          mingguAkhir: '31'
        })
        .then(function(result2) {
          result[i]['data'] = result2;
        })
        .catch(function(error) {
          return res.status(500).json(error);
        });
      }
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

};

module.exports = LaporanController;
