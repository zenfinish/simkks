const PersonnelEmployee = require('../models/personnel_employee.js');
const ZStase = require('../models/z_stase.js');
const mysql = require('../configs/mysql.js');

class MahasiswaController {

	static getSearch(req, res) {
		PersonnelEmployee.findBySearch(mysql, { search: req.headers.search })
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getStaseNpm(req, res) {
		ZStase.findByEmpCode(mysql, { emp_code: req.params.npm })
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postStase(req, res) {
		ZStase.createOne(mysql, req.body)
		.then(async function(result) {
      res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}
	
	static putStase(req, res) {
		PersonnelEmployee.updateByPositionId(mysql, {
			emp_id: req.body.emp_id,
			position_id: req.body.position_id,
		})
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}
	
	static putStaseNew(req, res) {
		ZStase.updateByidZstase(mysql, req.body)
		.then(async function(result) {
      res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}
	
	static deleteStaseIdZstase(req, res) {
		ZStase.deleteByidZstase(mysql, { id_zstase: req.params.id_zstase })
		.then(async function(result) {
      res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }

};

module.exports = MahasiswaController;
