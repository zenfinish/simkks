const PersonnelArea = require('../models/personnel_area.js');
const ZTarif = require('../models/z_tarif.js');
const mysql = require('../configs/mysql.js');

class RsController {

	static getAll(req, res) {
		PersonnelArea.findAll(mysql)
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}
	
	static putTarif(req, res) {
		ZTarif.saveOne(mysql, {
			name_personnelarea: req.body.area_name,
			tarif: req.body.tarif,
		})
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }

};

module.exports = RsController;
