const ZDokter = require('../models/z_dokter.js');
const ZUser = require('../models/z_user.js');
const ZNilai = require('../models/z_nilai.js');
const ZNilaiDet = require('../models/z_nilai_det.js');
const mysql = require('../configs/mysql.js');

class DokterController {

	static getAll(req, res) {
		ZDokter.findAll(mysql)
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getNilai(req, res) {
		ZNilai.findByIdZuser(mysql, { id_zuser: req.user.id_zuser })
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getNilaiDetilIdZNilai(req, res) {
		ZNilaiDet.findByIdZnilai(mysql, { id_znilai: req.params.id_znilai })
		.then(async function(result) {
      res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}
	
	static post(req, res) {
		mysql.getConnection(function(error, connection){
			if (error) {
				connection.release();
				return res.status(500).json(error.sqlMessage);	
			}
			connection.beginTransaction(function(error) {
				if (error) {
					connection.release();
					return res.status(500).json(error.sqlMessage);	
				}

				ZUser.saveOne(connection, req.body)
				.then(function(result) {
					req.body['id_zuser'] = result.insertId;

					ZDokter.saveOne(connection, req.body)
					.then(function(result) {
						connection.commit(function(error) {
							connection.release();
							res.status(201).json('Berhasil');
						});
					})
					.catch(function(error) {
						connection.rollback(function() {
							connection.release();
							res.status(500).json(error);
						});
					});
				})
				.catch(function(error) {
					connection.rollback(function() {
						connection.release();
						res.status(500).json(error);
					});
				});
			});
		});
		
	}
	
	static postNilai(req, res) {
		mysql.getConnection(function(error, connection){
			if (error) {
				connection.release();
				return res.status(500).json(error.sqlMessage);	
			}
			connection.beginTransaction(function(error) {
				if (error) {
					connection.release();
					return res.status(500).json(error.sqlMessage);	
				}

				ZDokter.findByIdZuser(connection, { id_zuser: req.user.id_zuser })
				.then(function(result) {
					req.body['area_id'] = result.area_id;
					req.body['id_zuser'] = req.user.id_zuser;

					ZNilai.saveOne(connection, req.body)
					.then(function(result) {
						let id_znilai = result.insertId;
						let values = [];
						for (let i = 0; i < req.body.data.length; i++) {
							values.push([id_znilai, req.body.data[i].emp_id, req.body.data[i].nilai]);
						}

						ZNilaiDet.saveMany(connection, values)
						.then(function(result) {
							connection.commit(function(error) {
								connection.release();
								res.status(201).json('Berhasil');
							});
						})
						.catch(function(error) {
							connection.rollback(function() {
								connection.release();
								res.status(500).json(error);
							});
						});
					})
					.catch(function(error) {
						connection.rollback(function() {
							connection.release();
							res.status(500).json(error);
						});
					});
				})
				.catch(function(error) {
					connection.rollback(function() {
						connection.release();
						res.status(500).json(error);
					});
				});
			});
		});
		
  }

};

module.exports = DokterController;
