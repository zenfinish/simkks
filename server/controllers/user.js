const { comparePassword } = require('../helpers/bcrypt.js');
const token = require('../configs/jsonwebtoken.js');
const PersonnelEmployee = require('../models/personnel_employee.js');
const mysql = require('../configs/mysql.js');

class UserController {

	static postLogin(req, res) {
		let checkPassword = comparePassword(req.body.password, req.body.sandi);
		if (checkPassword) {
			let obj = {
				id_zuser: req.body.id_zuser,
				nama_zuser: req.body.nama_zuser,
			};
			// obj['menus'] = result;
			token.createToken(obj, function(error, token) {
				if (error) {
					res.status(500).json(error);
				} else {
					res.status(200).json({
						token: token,
						datas: obj,
					});
				}
			});
		} else {
			res.status(500).json('Password Anda Salah.');
		};
	}

	static postLoginMahasiswa(req, res) {
		let obj = {
			npm: req.body.npm,
			nama_personnelemployee: req.body.nama_personnelemployee,
		};
		token.createToken(obj, function(error, token) {
			if (error) {
				res.status(500).json(error);
			} else {
				res.status(200).json({
					token: token,
					datas: obj,
				});
			}
		});
	}
	
	static getCektoken(req, res) {
		token.verifyToken(req.headers.token, function(error, decoded) {
			if (error) {
				res.status(500).json(error);
			} else {
				res.status(200).json(decoded);
			}
		});
	}
	
	static putMahasiswaPassword(req, res) {
		PersonnelEmployee.updatePasswordByEmpId(mysql, {
			password: req.body.password_baru,
			emp_code: req.body.emp_code,
		})
		.then(async function(result) {
      res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
  }

};

module.exports = UserController;
