const express = require('express');
const cors = require('cors');
const http = require('http');

require('dotenv').config();

const route = require('./routes/_index.js');

const app = express();
const PORT = process.env.PORT;

const httpServer = http.createServer(app);

app.use(cors());
app.use(express.json({ limit: '200mb' }));
app.use(express.urlencoded({ limit: '200mb', extended: false }));
app.use(route);

httpServer.listen(PORT, function() { console.log('Http Running On Port', PORT); });

module.exports = app;
