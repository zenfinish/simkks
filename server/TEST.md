USE zkteco12;

SELECT
	CONCAT('24-', '2020-08', ' s/d ', '31-', '2020-08') AS periode,
    c.dept_name AS kampus,
    CONCAT(COALESCE(b.first_name, ''), ' ', COALESCE(b.last_name, '')) AS nama,
    b.emp_code AS npm,
    e.area_name AS rumah_sakit,
    f.position_name AS stase,
    d.tarif,
    a.punch_time AS tgl
FROM iclock_transaction a
LEFT JOIN personnel_employee b ON a.emp_id = b.id
LEFT JOIN personnel_department c ON b.department_id = c.id
LEFT JOIN z_tarif d ON a.area_alias = d.name_personnelarea
LEFT JOIN personnel_area e ON d.name_personnelarea = e.area_name
LEFT JOIN personnel_position f ON a.id_personnelposition = f.id
WHERE
	a.punch_time BETWEEN CONCAT('2020-08', '-', '24') AND CONCAT('2020-08', '-', '31', ' 23:59:59')
GROUP BY SUBSTRING(a.punch_time, 1, 10), a.emp_id;