import React from 'react';
import api from 'configs/api.js';
import Add from './Add.jsx';
import Edit from './Edit.jsx';
import Delete from './Delete.jsx';
import { tglIndo } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
    data: [],
    openAdd: false,
    openEdit: false,
    openDelete: false,
    active: {},
  }

  componentDidMount() {
    this.refreshTable();
  }
  
  refreshTable = () => {
    api.get(`/mahasiswa/stase/${localStorage.getItem('npm')}`)
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}
	
	render() {
		return (
			<>
        <div className=" mb-1">
          <button
            className="border border-black"
            onClick={() => {
              this.setState({ openAdd: true });
            }}
          >Tambah Stase</button>
        </div>
        <table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm w-full">
          <tbody>
            <tr>
              <td className="border border-black">Tanggal Masuk</td>
              <td className="border border-black">Stase</td>
              <td className="border border-black">Rumah Sakit</td>
              <td className="border border-black">Dokter Pembimbing</td>
              <td className="border border-black">Action</td>
            </tr>
            {
              this.state.data.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{tglIndo(row.tgl_masuk)}</td>
                  <td className="border border-black">{row.nama_personnelposition}</td>
                  <td className="border border-black">{row.nama_personnelarea}</td>
                  <td className="border border-black">{row.nama_zdokter}</td>
                  <td className="border border-black">
                    <button
                      className="border border-black mr-1"
                      onClick={() => {
                        this.setState({ active: row, openEdit: true });
                      }}
                    >Edit</button>
                    <button
                      className="border border-black"
                      onClick={() => {
                        this.setState({ active: row, openDelete: true });
                      }}
                    >Delete</button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

        {
          this.state.openAdd ?
          <Add
            close={() => { this.setState({ openAdd: false }); }}
            closeRefresh={() => { this.setState({ openAdd: false }, () => {
              this.refreshTable();
            }); }}
            alert={this.props.alert}
          />
          : null
        }
        {
          this.state.openEdit ?
          <Edit
            data={this.state.active}
            close={() => { this.setState({ openEdit: false }); }}
            closeRefresh={() => { this.setState({ openEdit: false }, () => {
              this.refreshTable();
            }); }}
            alert={this.props.alert}
          />
          : null
        }
        {
          this.state.openDelete ?
          <Delete
            data={this.state.active}
            close={() => { this.setState({ openDelete: false }); }}
            closeRefresh={() => { this.setState({ openDelete: false }, () => {
              this.refreshTable();
            }); }}
            alert={this.props.alert}
          />
          : null
        }

      </>
		);
	}

};

export default Index;
