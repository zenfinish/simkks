import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class Delete extends React.Component {

	state = {
		dataPosition: [],
		dataArea: [],
		dataDokter: [],

		id_personnelposition: '',
		id_personnelarea: '',
		id_zdokter: '',
		tgl_masuk: '',
	}

	componentDidMount() {
		this.setState({
			id_personnelposition: this.props.data.id_personnelposition,
			id_personnelarea: this.props.data.id_personnelarea,
			id_zdokter: this.props.data.id_zdokter,
			tgl_masuk: this.props.data.tgl_masuk,
		}, () => {
			api.get(`/position/all`)
			.then(result => {
				this.setState({ dataPosition: result.data });
			})
			.catch(error => {
				console.log(error.response)
			});
	
			api.get(`/area/all`)
			.then(result => {
				this.setState({ dataArea: result.data });
			})
			.catch(error => {
				console.log(error.response)
			});
			
			api.get(`/dokter/all`)
			.then(result => {
				this.setState({ dataDokter: result.data });
			})
			.catch(error => {
				console.log(error.response)
			});
		});
		
	}

	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span>Delete Stase</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div className="mb-2">
						{this.props.data.nama_personnelposition}
					</div>
					<div className="mb-2">
						{this.props.data.nama_personnelarea}
					</div>
					<div className="mb-2">Tgl Masuk : {tglIndo(this.props.data.tgl_masuk)}</div>
					<div className="mb-2">
						{this.props.data.nama_zdokter}
					</div>
				</DialogContent>
				<DialogActions>
					<button
						className="border border-black"
						onClick={() => {
							api.delete(`/mahasiswa/stase/${this.props.data.id_zstase}`)
							.then(result => {
								this.props.closeRefresh();
							})
							.catch(error => {
								this.props.alert(JSON.stringify(error.response.data));
							});
						}}
					>Delete</button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default Delete;
