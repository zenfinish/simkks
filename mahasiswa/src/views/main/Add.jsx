import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';

class Add extends React.Component {

	state = {
		dataPosition: [],
		dataArea: [],
		dataDokter: [],

		id_personnelposition: '',
		id_personnelarea: '',
		id_zdokter: '',
		tgl_masuk: '',
	}

	componentDidMount() {
		api.get(`/position/all`)
    .then(result => {
      this.setState({ dataPosition: result.data });
    })
    .catch(error => {
      console.log(error.response)
		});

		api.get(`/area/all`)
    .then(result => {
      this.setState({ dataArea: result.data });
    })
    .catch(error => {
      console.log(error.response)
		});
		
		api.get(`/dokter/all`)
    .then(result => {
      this.setState({ dataDokter: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}

	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span>Tambah Stase</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div className="mb-2">
						<select
							onChange={(e) => {
								this.setState({ id_personnelposition: e.target.value });
							}}
							value={this.state.id_personnelposition}
							className="border border-black w-full mb-2"
						>
							<option value="">- Pilih Stase -</option>
							{
								this.state.dataPosition.map((row, i) => (
									<option key={i} value={row.position_id}>{row.position_name}</option>
								))  
							}
						</select>
					</div>
					<div className="mb-2">
						<select
							onChange={(e) => {
								this.setState({ id_personnelarea: e.target.value });
							}}
							value={this.state.id_personnelarea}
							className="border border-black w-full mb-2"
						>
							<option value="">- Pilih Rumah Sakit -</option>
							{
								this.state.dataArea.map((row, i) => (
									<option key={i} value={row.area_id}>{row.area_name}</option>
								))  
							}
						</select>
					</div>
					<div className="mb-2">Tgl Masuk</div>
					<div className="mb-2">
						<input
							type="date"
							className="w-full"
							onChange={(e) => {
								this.setState({ tgl_masuk: e.target.value });
							}}
							value={this.state.tgl_masuk}
						/>
					</div>
					<div className="mb-2">
						<select
							onChange={(e) => {
								this.setState({ id_zdokter: e.target.value });
							}}
							value={this.state.id_zdokter}
							className="border border-black w-full mb-2"
						>
							<option value="">- Pilih Dokter Pembimbing -</option>
							{
								this.state.dataDokter.map((row, i) => (
									<option key={i} value={row.id_zuser}>{row.nama_zuser}</option>
								))  
							}
						</select>
					</div>
				</DialogContent>
				<DialogActions>
					<button
						className="border border-black"
						onClick={() => {
							api.post(`/mahasiswa/stase`, {
								emp_code: localStorage.getItem('npm'),
								id_personnelarea: this.state.id_personnelarea,
								id_personnelposition: this.state.id_personnelposition,
								id_zdokter: this.state.id_zdokter,
								tgl_masuk: this.state.tgl_masuk,
							})
							.then(result => {
								this.props.closeRefresh();
							})
							.catch(error => {
								this.props.alert(JSON.stringify(error.response.data));
							});
						}}
					>Simpan</button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default Add;
