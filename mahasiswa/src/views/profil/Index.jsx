import React from 'react';
import api from 'configs/api.js';

class Index extends React.Component {

	state = {
    password_lama: '',
    ulangi_password_lama: '',
    password_baru: '',
  }

  componentDidMount() {
    this.refreshTable();
  }
  
  refreshTable = () => {
    api.get(`/mahasiswa/stase/${localStorage.getItem('npm')}`)
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
  }
  
  updatePassword = () => {
    if (this.state.password_lama === '' || this.state.password_baru === '') {
      this.props.alert('password masih kosong');
    } else if (this.state.password_lama !== this.state.ulangi_password_lama) {
      this.props.alert('ulangi password lama tidak sama dengan password lama');
    } else {
      api.put(`/user/mahasiswa/password`, {
        emp_code: localStorage.getItem('npm'),
        password_lama: this.state.password_lama,
        password_baru: this.state.password_baru,
      })
      .then(result => {
        api.defaults.headers['token'] = '';
        localStorage.removeItem('token');
        window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
      })
      .catch(error => {
        this.props.alert(JSON.stringify(error.response.data));
      });
    }
  }
	
	render() {
		return (
			<>
        <div className="mb-1">
          <input
            type="password"
            placeholder="Masukkan Password Lama"
            onChange={(e) => {
              this.setState({ password_lama: e.target.value });
            }}
          />
        </div>
        <div className="mb-1">
          <input
            type="password"
            placeholder="Ulangi Password Lama"
            onChange={(e) => {
              this.setState({ ulangi_password_lama: e.target.value });
            }}
          />
        </div>
        <div className="mb-1">
          <input
            type="password"
            placeholder="Masukkan Password Baru"
            onChange={(e) => {
              this.setState({ password_baru: e.target.value });
            }}
          />
        </div>
        <div>
          <button className="border border-black" onClick={this.updatePassword}>Update Password</button>
        </div>
      </>
		);
	}

};

export default Index;
