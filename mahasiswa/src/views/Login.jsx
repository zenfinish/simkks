import React from 'react';
import { Redirect } from 'react-router-dom'
import api from 'configs/api.js';

class Login extends React.Component {

	state = {
		redirect: false,
		open: false,
		bagian: 0,
		npm: '',
		password: '',
	}

	login = (e) => {
		e.preventDefault();
		this.setState({ open: true }, () => {
			api.post(`/user/login/mahasiswa`, {
				npm: this.state.npm,
				password: this.state.password,
			})
			.then(result => {
				api.defaults.headers['token'] = result.data.token;
				localStorage.setItem('token', result.data.token);
				localStorage.setItem('npm', result.data.datas.npm);
				localStorage.setItem('nama_personnelemployee', result.data.datas.nama_personnelemployee);
				this.setState({ redirect: true }); 
			})
			.catch(error => {
				this.props.alert(JSON.stringify(error.response.data), 'error');
				this.setState({ open: false })
				console.log('gagal: ', error)
			});
		});
	}
	
	render() {
		return (
			<div className="container mx-auto h-full flex justify-center items-center">
				<div className="w-1/3">
					<h1 className="font-hairline mb-6 text-center">SIM KKS LOGIN MAHASISWA</h1>
					<div className="border-teal p-8 border-t-12 bg-white mb-6 rounded-lg shadow-lg">
					<form onSubmit={this.login}>
						
						<div className="mb-4">
							<label className="block text-gray-700 text-sm font-bold mb-2">
								NPM
							</label>
							<input
								type="text"
								value={this.state.npm}
								onChange={(e) => {
									this.setState({ npm: e.target.value });
								}}
								placeholder="NPM"
								className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
							/>
						</div>
						
						<div className="mb-4">
							<label className="block text-gray-700 text-sm font-bold mb-2">
								Password
							</label>
							<input
								type="password"
								value={this.state.password}
								onChange={(e) => {
									this.setState({ password: e.target.value });
								}}
								placeholder="Password"
								className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
							/>
						</div>
						
						<div className="flex items-center justify-between">
							<button
								type="submit"
								disabled={this.state.open}
								className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
							>Log In</button>
						</div>
						
					</form>
					</div>
				</div>

				{
					this.state.redirect ?
						<Redirect to={`/main/index`} />
					: null
				}

			</div>
		);
	}

};

export default Login;
