import React, { useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import Alert  from 'components/Alert.jsx';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
   faCheckSquare,
   faCoffee,
   faChartBar,
   faUniversalAccess,
   faCubes,
   faBuilding,
   faFax,
   faUsersCog,
   faNewspaper,
   faSignOutAlt,
   faHospitalUser,
   faRegistered,
   faFileAlt,
   faUserMd,
   faProcedures,
   faWheelchair,
   faIdCard,
   faServer,
   faUsers,
   faMoneyCheckAlt,
   faCashRegister,
   faHome,
   faEdit,
   faTrashAlt,
   faShoppingCart,
   faFileInvoiceDollar,
} from '@fortawesome/free-solid-svg-icons';

import Login from './views/Login.jsx';
import Main from './views/Main.jsx';

library.add(
   faCheckSquare,
   faCoffee,
   faChartBar,
   faUniversalAccess,
   faCubes,
   faBuilding,
   faFax,
   faUsersCog,
   faNewspaper,
   faSignOutAlt,
   faHospitalUser,
   faRegistered,
   faFileAlt,
   faUserMd,
   faProcedures,
   faWheelchair,
   faIdCard,
   faServer,
   faUsers,
   faMoneyCheckAlt,
   faCashRegister,
   faHome,
   faEdit,
   faTrashAlt,
   faShoppingCart,
   faFileInvoiceDollar,
);

function App() {
   
   const [alert, setAlert] = useState({
      text: '',
      status: false,
   });
   
   return (
      <>
         <Switch>
            <Route exact path="/">
               <Login
                  alert={(text) => {
                     setAlert({
                        text: text,
                        status: true,
                     });
                  }}
               />
            </Route>
            <Route path="/main">
               <Main
                  alert={(text) => {
                     setAlert({
                        text: text,
                        status: true,
                     });
                  }}
               />
            </Route>
         </Switch>
         {
            alert.status ?
              <Alert text={alert.text} close={() => {
                setAlert({
                  text: '',
                  status: false,
                });
              }} />
          : null
         }
      </>
   );

}
  
export default (App);
