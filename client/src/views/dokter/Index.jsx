import React from 'react';
import api from 'configs/api.js';
import Add from './Add.jsx';

class Index extends React.Component {

	state = {
    data: [],
    openAdd: false,
    active: {},
  }

  componentDidMount() {
    this.refreshTable();
  }
  
  refreshTable = () => {
    api.get(`/dokter/all`)
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}
	
	render() {
		return (
			<>
        <div>
          <button
            className="border border-black"
            onClick={() => {
              this.setState({ openAdd: true });
            }}
          >Tambah Dokter</button>
        </div>
        <table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm w-full">
          <tbody>
            <tr>
              <td className="border border-black">Nama</td>
              <td className="border border-black">Handphone</td>
              <td className="border border-black">Rumah Sakit</td>
              <td className="border border-black">Email</td>
            </tr>
            {
              this.state.data.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{row.nama_zuser}</td>
                  <td className="border border-black">{row.hp}</td>
                  <td className="border border-black">{row.area_name}</td>
                  <td className="border border-black">{row.email}</td>
                </tr>
              ))
            }
          </tbody>
        </table>

        {
          this.state.openAdd ?
          <Add
            data={this.state.active}
            close={() => { this.setState({ openAdd: false }); }}
            closeRefresh={() => { this.setState({ openAdd: false }, () => {
              this.refreshTable();
            }); }}
            alert={this.props.alert}
          />
          : null
        }

      </>
		);
	}

};

export default Index;
