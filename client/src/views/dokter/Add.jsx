import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';

class Add extends React.Component {

	state = {
		nama_zuser: '',
		hp: '',
		email: '',
		area_id: '',
		dataArea: [],
	}

	componentDidMount() {
		api.get(`/area/all`)
    .then(result => {
      this.setState({ dataArea: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}

	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span>Tambah Dokter</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div>
						<h4>Nama Dokter</h4>
						<input
							onChange={(e) => {
								this.setState({ nama_zuser: e.target.value });
							}}
							value={this.state.nama_zuser}
							type="text"
							className="border border-black p-2"
						/>
					</div>
					<div>
						<h4>Handphone</h4>
						<input
							onChange={(e) => {
								this.setState({ hp: e.target.value });
							}}
							value={this.state.hp}
							type="text"
							className="border border-black p-2"
						/>
					</div>
					<div>
						<h4>Email</h4>
						<input
							onChange={(e) => {
								this.setState({ email: e.target.value });
							}}
							value={this.state.email}
							type="email"
							className="border border-black p-2"
						/>
					</div>
					<div>
						<h4>Rumah Sakit</h4>
						<select
							onChange={(e) => {
								this.setState({ area_id: e.target.value });
							}}
							value={this.state.area_id}
							className="border border-black"
						>
							<option value="">- Pilih RS -</option>
							{
								this.state.dataArea.map((row, i) => (
									<option key={i} value={row.area_id}>{row.area_name}</option>
								))  
							}
						</select>
					</div>
				</DialogContent>
				<DialogActions>
					<button
						className="border border-black"
						onClick={() => {
							api.post(`/dokter`, {
								nama_zuser: this.state.nama_zuser,
								hp: this.state.hp,
								email: this.state.email,
								area_id: this.state.area_id,
							})
							.then(result => {
								this.props.closeRefresh();
							})
							.catch(error => {
								this.props.alert(JSON.stringify(error.response.data));
							});
						}}
					>Simpan</button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default Add;
