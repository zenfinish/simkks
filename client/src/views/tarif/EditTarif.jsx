import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';

class EditTarif extends React.Component {

	state = {
		tarif: '',
	}

	componentDidMount() {
		this.setState({ tarif: this.props.data.tarif });
	}

	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span>Edit Stase {this.props.data.first_name} {this.props.data.last_name}</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<input
						onChange={(e) => {
							this.setState({ tarif: e.target.value });
						}}
						value={this.state.tarif}
						type="number"
						className="border border-black p-2"
					/>
				</DialogContent>
				<DialogActions>
					<button
						className="border border-black"
						onClick={() => {
							api.put(`/rs/tarif`, {
								area_name: this.props.data.area_name,
								tarif: this.state.tarif,
							})
							.then(result => {
								this.props.closeRefresh();
							})
							.catch(error => {
								this.props.alert(JSON.stringify(error.response.data));
							});
						}}
					>Update</button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default EditTarif;
