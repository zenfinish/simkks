import React from 'react';
import api from 'configs/api.js';
import EditTarif from './EditTarif.jsx';
import { separator } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
    data: [],
    openEditTarif: false,
    active: {},
  }

  componentDidMount() {
    this.refreshTable();
  }
  
  refreshTable = () => {
    api.get(`/rs/all`)
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}
	
	render() {
		return (
			<>
        <table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm w-full">
          <tbody>
            <tr>
              <td className="border border-black">Nama</td>
              <td className="border border-black">Tarif</td>
              <td className="border border-black">Action</td>
            </tr>
            {
              this.state.data.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{row.area_name}</td>
                  <td className="border border-black">{separator(row.tarif)}</td>
                  <td className="border border-black">
                    <button
                      className="border border-black"
                      onClick={() => {
                        this.setState({ active: row, openEditTarif: true });
                      }}
                    >Edit Tarif</button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

        {
          this.state.openEditTarif ?
          <EditTarif
            data={this.state.active}
            close={() => { this.setState({ openEditTarif: false }); }}
            closeRefresh={() => { this.setState({ openEditTarif: false }, () => {
              this.refreshTable();
            }); }}
            alert={this.props.alert}
          />
          : null
        }

      </>
		);
	}

};

export default Index;
