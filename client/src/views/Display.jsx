import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import api from 'configs/api.js';

class Display extends React.Component {

	state = {
		menus: [
			{ icon: 'file-alt', nama_menu: 'Data Stase Mahasiswa', link: '/stase' },
			{ icon: 'money-check-alt', nama_menu: 'Data Tarif Rumah Sakit', link: '/tarif' },
			{ icon: 'chart-bar', nama_menu: 'Laporan', link: '/laporan' },
			{ icon: 'user-md', nama_menu: 'Data Dokter', link: '/dokter' },
			{ icon: 'id-card', nama_menu: 'Penilaian Mahasiswa', link: '/nilai' },
		],
	}
	
	componentDidMount() {
		api.get(`/user/cektoken`)
		.catch(error => {
			console.log(error.response);
			window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
		});
	}
	
	render() {
		return (
			<div className="grid grid-cols-3 gap-x-6 gap-y-6 p-5">
					
				{
					this.state.menus.map((row, i) => (
						<div key={i}>
							<div align="center">
								<Link to={`main${row.link}`}>
									<FontAwesomeIcon
										icon={row.icon}
										// icon="universal-access"
										size="10x"
										className="hover:text-green-200 text-gray-300"
									/>
								</Link>
							</div>
							<div align="center" className="rounded-lg bg-teal-600 text-white px-4 py-2 mt-5">
								<span>{row.nama_menu}</span>
							</div>
						</div>
					))
				}

				<div>
					<div align="center">
						<FontAwesomeIcon
							icon="sign-out-alt"
							size="10x"
							className="hover:text-red-600 text-black cursor-pointer"
							onClick={() => {
								api.defaults.headers['token'] = '';
								localStorage.removeItem('token');
								window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
							}}
						/>
					</div>
					<div align="center" className="rounded-lg bg-black text-white px-4 py-2 mt-5">
						<span>Logout</span>
					</div>
				</div>

			</div>
		)
	}

};

export default (Display);
