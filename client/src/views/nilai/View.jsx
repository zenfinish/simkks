import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import { tglIndo } from 'configs/helpers.js';

class View extends React.Component {

	state = {
		data: [],
	}

	componentDidMount() {
		api.get(`/dokter/nilai/detil/${this.props.data.id_znilai}`)
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}

	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span>Detail Nilai</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div className="mb-2">
						<table>
							<tbody>
								<tr>
									<td>Tanggal</td>
									<td>:</td>
									<td>{tglIndo(this.props.data.tgl)}</td>
								</tr>
								<tr>
									<td>Nama Dokter</td>
									<td>:</td>
									<td>{this.props.data.nama_zuser}</td>
								</tr>
								<tr>
									<td>Rumah Sakit</td>
									<td>:</td>
									<td>{this.props.data.area_name}</td>
								</tr>
								<tr>
									<td>Stase</td>
									<td>:</td>
									<td>{this.props.data.position_name}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div style={{ height: '600px' }}>
						<table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm w-full">
							<tbody>
								<tr>
									<td className="border border-black">Nama Mahasiswa</td>
									<td className="border border-black">NPM</td>
									<td className="border border-black">Nilai</td>
								</tr>
								{
									this.state.data.map((row, i) => (
										<tr key={i}>
											<td className="border border-black">{row.first_name} {row.last_name}</td>
											<td className="border border-black">{row.npm}</td>
											<td className="border border-black">{row.nilai}</td>
										</tr>
									))
								}
							</tbody>
						</table>
					</div>
				</DialogContent>
				<DialogActions></DialogActions>
			</Dialog>
		)
	}

}

export default View;
