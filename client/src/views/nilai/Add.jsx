import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';
import Autocomplete from 'components/Autocomplete.jsx';

class Add extends React.Component {

	state = {
		dataSearchMahasiswa: [],
		dataMahasiswa: [],
		dataPosition: [],
		position_id: '',
	}

	componentDidMount() {
		api.get(`/position/all`)
    .then(result => {
      this.setState({ dataPosition: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}

	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span>Tambah Dokter</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<div className="mb-2">
						<select
							onChange={(e) => {
								this.setState({ position_id: e.target.value });
							}}
							value={this.state.position_id}
							className="border border-black w-full mb-2"
						>
							<option value="">- Pilih Stase -</option>
							{
								this.state.dataPosition.map((row, i) => (
									<option key={i} value={row.position_id}>{row.position_name}</option>
								))  
							}
						</select>
						<Autocomplete
							placeholder="Cari Mahasiswa"
							className="w-full"
							data={this.state.dataSearchMahasiswa}
							list={(value) => (
								<div>{value.first_name} {value.last_name} [{value.npm}]</div>
							)}
							onEnter={(value) => {
								api.get(`/mahasiswa/search`, {
									headers: { search: value }
								})
								.then(result => {
									const result2 = result.data.map((row) => {
										return {
											value: row.emp_id,
											first_name: row.first_name,
											last_name: !row.last_name ? '' : row.last_name,
											npm: row.npm,
											emp_id: row.emp_id,
										}
									})
									this.setState({ dataSearchMahasiswa: result2 });
								})
								.catch(error => {
									console.log(error.response)
								});
							}}
							onSelect={(value) => {
								let data = this.state.dataMahasiswa;
								data.push({ ...value, nilai: '' });
								this.setState({ dataMahasiswa: data });
							}}
						/>
					</div>
					<div style={{ height: '600px' }}>
						<table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm w-full">
							<tbody>
								<tr>
									<td className="border border-black">Nama Mahasiswa</td>
									<td className="border border-black">NPM</td>
									<td className="border border-black">Nilai</td>
								</tr>
								{
									this.state.dataMahasiswa.map((row, i) => (
										<tr key={i}>
											<td className="border border-black">{row.first_name} {row.last_name}</td>
											<td className="border border-black">{row.npm}</td>
											<td className="border border-black">
												<input
													type="text"
													value={row.nilai}
													onChange={(e) => {
														let data = this.state.dataMahasiswa;
														data[i]['nilai'] = e.target.value;
														this.setState({ dataMahasiswa: data });
													}}
													className="border border-black p-3"
													placeholder="-"
												/>
											</td>
										</tr>
									))
								}
							</tbody>
						</table>
					</div>
				</DialogContent>
				<DialogActions>
					<button
						className="border border-black"
						onClick={() => {
							api.post(`/dokter/nilai`, {
								position_id: this.state.position_id,
								data: this.state.dataMahasiswa,
							})
							.then(result => {
								this.props.closeRefresh();
							})
							.catch(error => {
								this.props.alert(JSON.stringify(error.response.data));
							});
						}}
					>Simpan</button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default Add;
