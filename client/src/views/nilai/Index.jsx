import React from 'react';
import api from 'configs/api.js';
import Add from './Add.jsx';
import View from './View.jsx';
import { tglIndo } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
    data: [],
    openAdd: false,
    openView: false,
    active: {},
  }

  componentDidMount() {
    this.refreshTable();
  }
  
  refreshTable = () => {
    api.get(`/dokter/nilai`)
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}
	
	render() {
		return (
			<>
        <div>
          <button
            className="border border-black"
            onClick={() => {
              this.setState({ openAdd: true });
            }}
          >Tambah Nilai</button>
        </div>
        <table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm w-full">
          <tbody>
            <tr>
              <td className="border border-black" align="right">Tanggal</td>
              <td className="border border-black">Rumah Sakit</td>
              <td className="border border-black">Stase</td>
              <td className="border border-black">Action</td>
            </tr>
            {
              this.state.data.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black" align="right">{tglIndo(row.tgl)}</td>
                  <td className="border border-black">{row.area_name}</td>
                  <td className="border border-black">{row.position_name}</td>
                  <td className="border border-black">
                    <button
                      className="border border-black"
                      onClick={() => {
                        this.setState({ active: row, openView: true });
                      }}
                    >Lihat</button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

        {
          this.state.openAdd ?
          <Add
            data={this.state.active}
            close={() => { this.setState({ openAdd: false }); }}
            closeRefresh={() => { this.setState({ openAdd: false }, () => {
              this.refreshTable();
            }); }}
            alert={this.props.alert}
          />
          : null
        }
        {
          this.state.openView ?
          <View
            data={this.state.active}
            close={() => { this.setState({ openView: false }); }}
          />
          : null
        }

      </>
		);
	}

};

export default Index;
