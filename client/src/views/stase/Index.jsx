import React from 'react';
import api from 'configs/api.js';
import EditStase from './EditStase.jsx';

class Index extends React.Component {

	state = {
    data: [],
    search: '',
    openEditStase: false,
    active: {},
  }
  
  refreshTable = () => {
    api.get(`/mahasiswa/search`, {
      headers: { search: this.state.search }
    })
    .then(result => {
      this.setState({ data: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
	}
	
	render() {
		return (
			<>
        <div className="mb-3">
          <input
            type="text"
            onChange={(e) => {
              this.setState({ search: e.target.value });
            }}
            className="border border-black w-full p-2"
            placeholder="Cari Berdasarkan Nama / NPM ..."
            onKeyUp={(e) => {
              if (e.keyCode === 13 && this.state.search.length >= 3) this.refreshTable();
            }}
          />
        </div>
        <table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm w-full">
          <tbody>
            <tr>
              <td className="border border-black">Nama</td>
              <td className="border border-black">NPM</td>
              <td className="border border-black">Kampus</td>
              <td className="border border-black">Stase</td>
              <td className="border border-black">Action</td>
            </tr>
            {
              this.state.data.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{row.first_name} {row.last_name}</td>
                  <td className="border border-black">{row.npm}</td>
                  <td className="border border-black">{row.kampus}</td>
                  <td className="border border-black">{row.stase}</td>
                  <td className="border border-black">
                    <button
                      className="border border-black"
                      onClick={() => {
                        this.setState({ active: row, openEditStase: true });
                      }}
                    >Edit Stase</button>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>

        {
          this.state.openEditStase ?
          <EditStase
            data={this.state.active}
            close={() => { this.setState({ openEditStase: false }); }}
            closeRefresh={() => { this.setState({ openEditStase: false }, () => {
              this.refreshTable();
            }); }}
            alert={this.props.alert}
          />
          : null
        }

      </>
		);
	}

};

export default Index;
