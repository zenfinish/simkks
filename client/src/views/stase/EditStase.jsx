import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, DialogActions } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'configs/api.js';

class EditStase extends React.Component {

	state = {
		dataStase: [],
		position_id: '',
	}

	componentDidMount() {
		api.get(`/position/all`)
    .then(result => {
      this.setState({ dataStase: result.data, position_id: this.props.data.position_id });
    })
    .catch(error => {
      console.log(error.response)
    });
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="md" fullWidth scroll="paper">
				<DialogTitle>
					<span>Edit Stase {this.props.data.first_name} {this.props.data.last_name}</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<select onChange={(e) => {
						this.setState({ position_id: e.target.value });
					}} value={this.state.position_id}>
						<option value="">- Pilih Stase -</option>
						{
							this.state.dataStase.map((row, i) => (
								<option value={row.position_id} key={i}>{row.position_name}</option>
							))
						}
					</select>
				</DialogContent>
				<DialogActions>
					<button
						className="border border-black"
						onClick={() => {
							api.put(`/mahasiswa/stase`, {
								emp_id: this.props.data.emp_id,
								position_id: this.state.position_id,
							})
							.then(result => {
								this.props.closeRefresh();
							})
							.catch(error => {
								this.props.alert(JSON.stringify(error.response.data));
							});
						}}
					>Update</button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default EditStase;
