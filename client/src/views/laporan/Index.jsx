import React from 'react';
import api from 'configs/api.js';
import { separator } from 'configs/helpers.js';

class Index extends React.Component {

	state = {
    data1: [],
    data2: [],
    data3: [],
    data4: [],
    bulan: '',
    department_id: '',
    dataDepartment: [],
  }
  
  componentDidMount() {
    api.get(`/department/all`)
    .then(result => {
      this.setState({ dataDepartment: result.data });
    })
    .catch(error => {
      console.log(error.response)
    });
  }
  
  refreshTable = () => {
    this.setState({ data1: [], data2: [], data3: [], data4: [] }, () => {
      api.get(`/laporan/${this.state.bulan}/minggu1/${this.state.department_id}`)
      .then(result => {
        // const guguk = result.data.reduce((acc, curr, i) => {
        //   const key = curr.npm;
        //   if (!acc[key]) {
        //     acc[key] = []
        //   } 
        //   acc[key].push(curr);
        //   console.log('masuk===', acc)
        //   return acc;
        // }, []);
        this.setState({ data1: result.data });
      })
      .catch(error => {
        console.log(error.response)
      });

      api.get(`/laporan/${this.state.bulan}/minggu2/${this.state.department_id}`)
      .then(result => {
        this.setState({ data2: result.data });
      })
      .catch(error => {
        console.log(error.response)
      });

      api.get(`/laporan/${this.state.bulan}/minggu3/${this.state.department_id}`)
      .then(result => {
        this.setState({ data3: result.data });
      })
      .catch(error => {
        console.log(error.response)
      });

      api.get(`/laporan/${this.state.bulan}/minggu4/${this.state.department_id}`)
      .then(result => {
        this.setState({ data4: result.data });
      })
      .catch(error => {
        console.log(error.response)
      });
    });
	}
	
	render() {
		return (
			<>
        <div>
          <input
            type="month"
            onChange={(e) => {
              this.setState({ bulan: e.target.value });
            }}
            className="border border-black"
          />
          <select
            onChange={(e) => {
              this.setState({ department_id: e.target.value });
            }}
            value={this.state.department_id}
            className="border border-black"
          >
            <option value="">- Pilih Kampus -</option>
            {
              this.state.dataDepartment.map((row, i) => (
                <option key={i} value={row.department_id}>{row.dept_name}</option>
              ))  
            }
          </select>
          <button
            onClick={this.refreshTable}
            className="border border-black"
          >Cari</button>
        </div>
        <table border="1" cellPadding={3} style={{ whiteSpace: 'nowrap' }} className="text-sm">
          <tbody>
            <tr>
              <td className="border border-black">Periode Minggu</td>
              <td className="border border-black">Asal Kampus</td>
              <td className="border border-black">Nama</td>
              <td className="border border-black">NPM</td>
              <td className="border border-black">Rumah Sakit</td>
              <td className="border border-black">Stase</td>
              <td className="border border-black">Jumlah Stase</td>
              <td className="border border-black">Tarif Harian</td>
              <td className="border border-black">Total</td>
              <td className="border border-black">Tanggal Hadir</td>
            </tr>
            {
              this.state.data1.length !== 0 ?
              <tr><td colSpan={10}>Minggu Ke 1</td></tr> : null
            }
            {
              this.state.data1.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{row.periode}</td>
                  <td className="border border-black">{row.kampus}</td>
                  <td className="border border-black">{row.nama}</td>
                  <td className="border border-black">{row.npm}</td>
                  <td className="border border-black">{row.rumah_sakit}</td>
                  <td className="border border-black">{row.stase}</td>
                  <td className="border border-black"></td>
                  <td className="border border-black">{separator(row.tarif)}</td>
                  <td className="border border-black">{separator(row.tarif * row.data.length)}</td>
                  <td className="border border-black">{row.data.map((row2) => { return row2.tgl.substring(8, 10) }).join(', ')}</td>
                </tr>
              ))
            }
            {
              this.state.data2.length !== 0 ?
              <tr><td colSpan={10}>Minggu Ke 2</td></tr> : null
            }
            {
              this.state.data2.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{row.periode}</td>
                  <td className="border border-black">{row.kampus}</td>
                  <td className="border border-black">{row.nama}</td>
                  <td className="border border-black">{row.npm}</td>
                  <td className="border border-black">{row.rumah_sakit}</td>
                  <td className="border border-black">{row.stase}</td>
                  <td className="border border-black"></td>
                  <td className="border border-black">{separator(row.tarif)}</td>
                  <td className="border border-black">{separator(row.tarif * row.data.length)}</td>
                  <td className="border border-black">{row.data.map((row2) => { return row2.tgl.substring(8, 10) }).join(', ')}</td>
                </tr>
              ))
            }
            {
              this.state.data3.length !== 0 ?
              <tr><td colSpan={10}>Minggu Ke 3</td></tr> : null
            }
            {
              this.state.data3.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{row.periode}</td>
                  <td className="border border-black">{row.kampus}</td>
                  <td className="border border-black">{row.nama}</td>
                  <td className="border border-black">{row.npm}</td>
                  <td className="border border-black">{row.rumah_sakit}</td>
                  <td className="border border-black">{row.stase}</td>
                  <td className="border border-black"></td>
                  <td className="border border-black">{separator(row.tarif)}</td>
                  <td className="border border-black">{separator(row.tarif * row.data.length)}</td>
                  <td className="border border-black">{row.data.map((row2) => { return row2.tgl.substring(8, 10) }).join(', ')}</td>
                </tr>
              ))
            }
            {
              this.state.data4.length !== 0 ?
              <tr><td colSpan={10}>Minggu Ke 4</td></tr> : null
            }
            {
              this.state.data4.map((row, i) => (
                <tr key={i}>
                  <td className="border border-black">{row.periode}</td>
                  <td className="border border-black">{row.kampus}</td>
                  <td className="border border-black">{row.nama}</td>
                  <td className="border border-black">{row.npm}</td>
                  <td className="border border-black">{row.rumah_sakit}</td>
                  <td className="border border-black">{row.stase}</td>
                  <td className="border border-black"></td>
                  <td className="border border-black">{separator(row.tarif)}</td>
                  <td className="border border-black">{separator(row.tarif * row.data.length)}</td>
                  <td className="border border-black">{row.data.map((row2) => { return row2.tgl.substring(8, 10) }).join(', ')}</td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </>
		);
	}

};

export default Index;
