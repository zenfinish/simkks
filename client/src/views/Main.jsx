import React from 'react';
import { Route } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import api from 'configs/api.js';

import Laporan from 'views/laporan/Index.jsx';
import Stase from 'views/stase/Index.jsx';
import Tarif from 'views/tarif/Index.jsx';
import Dokter from 'views/dokter/Index.jsx';
import Nilai from 'views/nilai/Index.jsx';

class Main extends React.Component {

	componentDidMount() {
		api.get(`/user/cektoken`)
		.catch(error => {
			window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
		});
	}
	
	render() {
		return (
			<>
				<div className="px-6	py-3">
					<FontAwesomeIcon
						icon="home"
						className="hover:text-green-200 mr-1 cursor-pointer"
						onClick={() => {
							this.props.history.push(`/display`);
						}}
						title="Home"
					/>
					<FontAwesomeIcon
						icon="sign-out-alt"
						className="hover:text-green-200 mr-1 cursor-pointer"
						onClick={() => {
							api.defaults.headers['token'] = '';
							localStorage.removeItem('token');
							window.location.replace(`${process.env.REACT_APP_PUBLIC_URL}`);
						}}
						title="Logout"
					/>
				</div>
				<div
					className="px-6	py-3 bg-gray-100 fixed overflow-auto"
					style={{ left: 0, top: '3rem', bottom: 30, right: 0 }}
				>
					<div className="w-full relative h-full">
						<Route path="/main/laporan"><Laporan alert={this.props.alert} /></Route>
						<Route path="/main/stase"><Stase alert={this.props.alert} /></Route>
						<Route path="/main/tarif"><Tarif alert={this.props.alert} /></Route>
						<Route path="/main/dokter"><Dokter alert={this.props.alert} /></Route>
						<Route path="/main/nilai"><Nilai alert={this.props.alert} /></Route>
					</div>
				</div>
			</>
		)
	}

};

export default withRouter(Main);
